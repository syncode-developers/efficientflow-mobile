package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.vishnusivadas.advanced_httpurlconnection.PutData

class MainActivity : AppCompatActivity() {

    private lateinit var usernameEditText: EditText
    private lateinit var passwordEditText: EditText
    private lateinit var loginButton: Button
    private lateinit var signUpT: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Initialize views
        usernameEditText = findViewById(R.id.username)
        passwordEditText = findViewById(R.id.password)
        loginButton = findViewById(R.id.loginButton)
        signUpT = findViewById(R.id.signUpT)

        signUpT.setOnClickListener {

            val intent = Intent(this@MainActivity, SignUpActivity::class.java)
            startActivity(intent)
            finish()
        }

        loginButton.setOnClickListener {


            val username = usernameEditText.text.toString()
            val password = passwordEditText.text.toString()


            val combinedString = "$username, $password"

            if (!username.equals("") && !password.equals("")) {
                val handler = Handler(Looper.getMainLooper())
                handler.post(Runnable {
                    val field = arrayOfNulls<String>(2)
                    field[0] = "username"
                    field[1] = "password"

                    // Creating array for data
                    val data = arrayOfNulls<String>(2)
                    data[0] = username
                    data[1] = password

                    val putData = PutData(
                        "http://192.168.5.157/LoginRegister/login.php", "POST", field, data)

                    if (putData.startPut()) {
                        if (putData.onComplete()) {
                            val result: String = putData.getResult()
                            if (result.equals("Login Success")) {
                                Toast.makeText(applicationContext,
                                    "Login Success", Toast.LENGTH_SHORT).show()
                                val intent = Intent(applicationContext, Menu::class.java)
                                startActivity(intent)
                                finish()

                            } else {
                                // Display an error message when login fails
                                Toast.makeText(applicationContext, result, Toast.LENGTH_SHORT).show()

                            }
                        }
                    }
                })
            } else {
                Toast.makeText(applicationContext, "All fields are required", Toast.LENGTH_SHORT).show()
            }
        }
    }
}