package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.vishnusivadas.advanced_httpurlconnection.PutData


class SignUpActivity : AppCompatActivity() {

    private lateinit var usernameEditText: EditText
    private lateinit var fullnameEditText: EditText
    private lateinit var passwordEditText: EditText
    private lateinit var emailEditText: EditText
    private lateinit var signUpButton: Button
    private lateinit var log2: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        usernameEditText = findViewById(R.id.username)
        fullnameEditText = findViewById(R.id.fullname)
        passwordEditText = findViewById(R.id.password)
        emailEditText = findViewById(R.id.email)
        signUpButton = findViewById(R.id.signUpButton)

        log2 = findViewById(R.id.log2   )

        log2.setOnClickListener {
            // Handle the click event here, for example, navigate to a signup activity
            val intent = Intent(this@SignUpActivity, MainActivity::class.java)
            startActivity(intent)
            finish()
        }


        signUpButton.setOnClickListener {

            val fullname = fullnameEditText.text.toString()
            val username = usernameEditText.text.toString()
            val password = passwordEditText.text.toString()
            val email = emailEditText.text.toString()

            val combinedString = "$fullname, $username, $password, $email"

            if (!fullname.equals("") && !username.equals("") && !password.equals("") && !email.equals(
                    ""
                )
            ) {

                val handler = Handler(Looper.getMainLooper())
                handler.post(Runnable {

                    val field = arrayOfNulls<String>(4)
                    field[0] = "fullname"
                    field[1] = "username"
                    field[2] = "password"
                    field[3] = "email"
                    //Creating array for data
                    val data = arrayOfNulls<String>(4)
                    data[0] = fullname
                    data[1] = username
                    data[2] = password
                    data[3] = email

                    val putData = PutData(
                        "http://192.168.5.157/LoginRegister/signup.php",
                        "POST",
                        field,
                        data
                    )
                    if (putData.startPut()) {
                        if (putData.onComplete()) {
                            val result: String = putData.getResult()
                            if (result.equals("Sign Up Success")) {
                                Toast.makeText(applicationContext, "Sign Up Success", Toast.LENGTH_SHORT).show()
                                intent = Intent(applicationContext, MainActivity::class.java)
                                startActivity(intent)
                                finish()
                            }
                            else{
                                Toast.makeText(applicationContext, result,Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                    //End Write and Read data with URL
                })
            }
            else{
                Toast.makeText(applicationContext, "All fields are required", Toast.LENGTH_SHORT).show()

            }
        }
    }
}

