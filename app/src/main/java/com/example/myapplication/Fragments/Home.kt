package com.example.myapplication.Fragments

import android.app.DatePickerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.myapplication.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.HttpURLConnection
import java.net.URL
import java.nio.charset.Charset
import java.util.Calendar

class Home : Fragment() {
    private lateinit var productName: EditText
    private lateinit var price: EditText
    private lateinit var expiration: EditText
    private lateinit var stockQuantity: EditText
    private lateinit var supplierName: EditText
    private lateinit var saveButton: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        // Initialize UI elements
        productName = view.findViewById(R.id.productName)
        price = view.findViewById(R.id.price)
        expiration = view.findViewById(R.id.expiration)
        stockQuantity = view.findViewById(R.id.stockQuantity)
        supplierName = view.findViewById(R.id.supplierName)
        saveButton = view.findViewById(R.id.saveButton)

        // Set an onClickListener for the save button
        saveButton.setOnClickListener {
            saveProduct()
        }

        // Set an onClickListener for the expiration EditText to show the date picker
        expiration.setOnClickListener {
            showDatePickerDialog()
        }

        return view
    }

    private fun showDatePickerDialog() {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(
            requireContext(),
            { _, selectedYear, selectedMonth, selectedDay ->
                // Handle the selected date
                val formattedDate = "$selectedYear-${selectedMonth + 1}-$selectedDay"
                expiration.setText(formattedDate)
            },
            year,
            month,
            dayOfMonth
        )

        // Show the date picker dialog
        datePickerDialog.show()
    }

    private fun saveProduct() {
        val productName = productName.text.toString()
        val price = price.text.toString()
        val expiration = expiration.text.toString()
        val stockQuantity = stockQuantity.text.toString()
        val supplierName = supplierName.text.toString()

        // Use a coroutine to run the network request on a background thread
        GlobalScope.launch(Dispatchers.IO) {
            val url = "http://192.168.5.157/LoginRegister/products.php"

            val connection = URL(url).openConnection() as HttpURLConnection
            connection.requestMethod = "POST"
            connection.doOutput = true

            // Prepare data to send to the server
            val postData =
                "productName=$productName&price=$price&expiration=$expiration&stockQuantity=$stockQuantity&supplierName=$supplierName"
            val outputStream = connection.outputStream
            outputStream.write(postData.toByteArray(Charset.forName("UTF-8")))

            // Close the output stream here
            outputStream.close()

            val responseCode = connection.responseCode
            if (responseCode == HttpURLConnection.HTTP_OK) {

                withContext(Dispatchers.Main) {
                    val successMessage = "Product saved successfully!"
                    Toast.makeText(requireContext(), successMessage, Toast.LENGTH_SHORT).show()


                    val navController = findNavController()
                    navController.navigate(R.id.id_dashboard_frag)
                }
            } else {

                withContext(Dispatchers.Main) {
                    val errorMessage = "Failed to save product. Response code: $responseCode"
                    Toast.makeText(requireContext(), errorMessage, Toast.LENGTH_SHORT).show()
                }
            }

            connection.disconnect()
        }
    }
}
