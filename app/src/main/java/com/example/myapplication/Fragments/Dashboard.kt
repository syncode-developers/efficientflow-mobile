package com.example.myapplication.Fragments

import ProductAdapter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.myapplication.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL



data class Product(
    val productName: String,
    val price: String,
    val expiration: String,
    val stockQuantity: String,
    val supplier: String
)

class Dashboard : Fragment() {
    private lateinit var productListView: ListView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_dashboard, container, false)
        productListView = view.findViewById(R.id.productListView)

        // Retrieve the list of products from the server
        retrieveProducts()

        return view
    }

    private fun retrieveProducts() {
        GlobalScope.launch(Dispatchers.IO) {
            try {
                val url = "http://192.168.5.157/LoginRegister/products.php"
                val connection = URL(url).openConnection() as HttpURLConnection
                connection.requestMethod = "GET"

                val responseCode = connection.responseCode
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    val reader = BufferedReader(InputStreamReader(connection.inputStream))
                    val response = StringBuilder()
                    var line: String?

                    while (reader.readLine().also { line = it } != null) {
                        response.append(line)
                    }

                    withContext(Dispatchers.Main) {
                        // Handle the JSON response
                        handleProductResponse(response.toString())
                    }
                } else {
                    withContext(Dispatchers.Main) {
                        val errorMessage =
                            "Failed to retrieve products. Response code: $responseCode"
                        Toast.makeText(requireContext(), errorMessage, Toast.LENGTH_SHORT).show()
                    }
                }

                connection.disconnect()
            } catch (e: Exception) {
                e.printStackTrace()
                withContext(Dispatchers.Main) {
                    val errorMessage = "Exception while fetching data: ${e.message}"
                    Toast.makeText(requireContext(), errorMessage, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun handleProductResponse(jsonResponse: String) {
        try {
            val jsonObject = JSONObject(jsonResponse)
            if (jsonObject.getBoolean("success")) {
                val productsArray = jsonObject.getJSONArray("products")
                val products = mutableListOf<Product>()

                for (i in productsArray.length() - 1 downTo 0) { // Traverse the products in reverse order
                    val productObject = productsArray.getJSONObject(i)
                    val productName = productObject.getString("product_name")
                    val price = productObject.getString("price")
                    val expiration = productObject.getString("expiration")
                    val stockQuantity = productObject.getString("stock_quantity")
                    val supplierName = productObject.getString("supplier_name")

                    val product =
                        Product(productName, price, expiration, stockQuantity, supplierName)
                    products.add(product)
                }

                // Create a custom adapter for the ListView
                val adapter = ProductAdapter(requireContext(), products)

                // Set the adapter for the ListView
                productListView.adapter = adapter
            } else {
                // Handle the case where "success" is false (e.g., an error occurred on the server)
                val errorMessage =
                    "Failed to retrieve products. Message: ${jsonObject.getString("message")}"
                Toast.makeText(requireContext(), errorMessage, Toast.LENGTH_SHORT).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}