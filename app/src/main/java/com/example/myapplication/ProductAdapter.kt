import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.myapplication.Fragments.Product
import com.example.myapplication.R

class ProductAdapter(context: Context, private val products: List<Product>) : ArrayAdapter<Product>(context, 0, products) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = convertView ?: LayoutInflater.from(context).inflate(R.layout.list_item_product, parent, false)
        val product = products[position]

        val productName = view.findViewById<TextView>(R.id.productName)
        val price = view.findViewById<TextView>(R.id.price)
        val expiration = view.findViewById<TextView>(R.id.expiration)
        val stockQuantity = view.findViewById<TextView>(R.id.stockQuantity)
        val supplierName = view.findViewById<TextView>(R.id.supplierName)

        productName.text = "Product Name: ${product.productName}"

        // Convert the price to a decimal and format it with two decimal places
        val priceString = product.price
        val priceDouble = priceString.toDouble()
        price.text = "Price: ${String.format("%.2f", priceDouble)}" // Display with two decimal places

        expiration.text = "Expiration Date: ${product.expiration}"
        stockQuantity.text = "Stock Quantity: ${product.stockQuantity}"
        supplierName.text = "Supplier: ${product.supplier}"

        return view
    }
}
